"use strict";

app.config( ['$routeProvider', 
    function( $routeProvider ){

        $routeProvider
             .when(
                "/part1",
                {
                    action: "part1.list"
                }
            )

            .when(
                "/",
                {
                    redirectTo: "/part1"
                }
            )
            .when(
                "/story",
                {

                    action: "story.main"
                }
            )
            .when(
                "/part2",
                {
                    action: "part2.list"
                }
            )
        .when(
                "/part3",
                {
                    action: "part3.list"
                }
            )

           /*
            .when(
                "/incumbents/:_name",
                {
                    action: "incumbents.list"
                }
            )*/
            /*.when(
                "/reset/:_name",
                {
                    action: "reset.item"
                }
            )*/
            .otherwise(
                {
                    redirectTo: "/"
                }
            )
        ;
        //$locationProvider.html5Mode(true);
        //$locationProvider.hashPrefix('!');

    }]
);